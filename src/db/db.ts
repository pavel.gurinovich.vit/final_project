import mongoose from "mongoose"
import config from 'config'

const host = config.get('dbConfig.host')
const PORT = config.get('dbConfig.PORT')
const dbName = config.get('dbConfig.dbName')

const db = mongoose.connect(
    `mongodb://${host}:${PORT}/${dbName}`,
    { useUnifiedTopology: true, useNewUrlParser: true, useFindAndModify: false},
    (err)=>{
        if(!err){
            console.log("MongoDB Connection Succeeded")
        }else{
            console.log("Error in DB connection: " + err)
        }
    })

export default db
