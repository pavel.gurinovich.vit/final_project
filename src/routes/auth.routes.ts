import express from 'express'
import { signin, login } from '../controllers/auth.controller';

const router: any = express.Router()

router.post("/registration", signin);
router.post("/login", login);


export default router