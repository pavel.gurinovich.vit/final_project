import express from 'express'
import { create_post,get_by_name, get_all, edit_post, delete_post } from '../controllers/post.controller';

const router: any = express.Router()

router.post("/create", create_post);
router.get("/getbyname", get_by_name);
router.get("/getall", get_all);
router.put("/:id", edit_post);
router.delete("/:id", delete_post);

export default router