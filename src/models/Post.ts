import {Schema, model} from 'mongoose'

interface IPost {
    content: Object,
    user: Schema.Types.ObjectId
}

const schema = new Schema<IPost>({
    content: {
        type: Object
    },

    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
    }
})

const Post = model<IPost>('Order', schema)

export {Post, IPost}