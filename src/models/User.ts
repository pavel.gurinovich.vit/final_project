import {Schema, model} from 'mongoose'

interface IUser {
    email: string,
    password: string,
    username: string,
    posts: Array<Schema.Types.ObjectId>

}

const schema = new Schema<IUser>({
    email: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true,
    },
    username: {
        type: String,
        required: true,
        unique: true
    },
    posts: [{
        type: Schema.Types.ObjectId,
        ref: 'Post'
    }]
  
})

const User = model<IUser>('User', schema)
export {User, IUser}