import express from 'express';
import {json} from 'body-parser'
import db from './src/db/db'
import router from './src/routes/routes'
import config  from 'config';

const PORT = config.get('app.PORT')
const app = express();


app.use('/', router)

const start = async () =>{
  try{
      await db
      app.listen(PORT, ()=>[
          console.log(`Server is running on port: ${PORT}`)
      ]);
  }catch (e){
      console.log(e);
  }
}

start()